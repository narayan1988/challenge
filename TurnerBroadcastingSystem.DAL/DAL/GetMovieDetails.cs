﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using TurnerBroadcastingSystem.Models;

namespace TurnerBroadcastingSystem.DAL
{
    /// <summary>
    /// This calss is used to implement methods to retrive movies list and movie details.
    /// </summary>
    public class GetMovieDetails
    {
        /// <summary>
        /// This method is used to get list of movies name based on title name passed.
        /// </summary>
        /// <param name="movieName">string</param>
        /// <returns>List<String> object</returns>
        public List<String> GetMovieList(string movieName)
        {
            List<String> moviesList = new List<string>();
            try
            {
                TitlesEntities titlesEntities = new TitlesEntities();
                moviesList = (from titles in titlesEntities.Titles
                              where titles.TitleName.Contains(movieName)
                              select titles.TitleName).ToList();
            }
            catch (Exception e)
            {
                LogError(e.Message);
            }
            return moviesList;
        }

        /// <summary>
        /// This method is used to retrive movie details based on given movie title.
        /// </summary>
        /// <param name="movieName">string</param>
        /// <returns>List<MovieDetails> object</returns>
        public List<MovieDetails> GetMovieDetailsList(string movieName)
        {
            List<MovieDetails> movieDetailsList = new List<MovieDetails>();
            try
            {
                TitlesEntities titlesEntities = new TitlesEntities();
                movieDetailsList = (from title in titlesEntities.Titles
                                    join story in titlesEntities.StoryLines
                                        on title.TitleId equals story.TitleId
                                    join award in titlesEntities.Awards
                                        on title.TitleId equals award.TitleId
                                    join otherName in titlesEntities.OtherNames
                                        on title.TitleId equals otherName.TitleId
                                    where title.TitleId == titlesEntities.Titles.FirstOrDefault(x => x.TitleName == movieName).TitleId
                                    select new MovieDetails
                                    {
                                        TitleName = title.TitleName,
                                        ReleaseYear = title.ReleaseYear,
                                        Language = story.Language,
                                        Description = story.Description,
                                        AwardWon = award.AwardWon == false ? true : false,
                                        AwardYear = award.AwardYear,
                                        Award = award.Award1,
                                        AwardCompany = award.AwardCompany
                                    }).ToList<MovieDetails>();

            }
            catch (Exception e)
            {
                LogError(e.Message);
            }

            return movieDetailsList;
        }

        /// <summary>
        /// This method is used to log error details
        /// </summary>
        /// <param name="errorMessage">string</param>
        private void LogError(string errorMessage)
        {
            string fileLocation = ConfigurationManager.AppSettings["LogFilePath"].ToString();
            if (!File.Exists(fileLocation))
            {
                using (StreamWriter logWriter = File.CreateText(fileLocation))
                {
                    logWriter.WriteLine(DateTime.Now +" " + errorMessage);
                    logWriter.Close();
                }
            }
            else
            {
                using (StreamWriter logWriter = File.CreateText(fileLocation))
                {
                    logWriter.WriteLine(DateTime.Now + " " + errorMessage);
                    logWriter.Close();
                }
            }
        }
    }
}