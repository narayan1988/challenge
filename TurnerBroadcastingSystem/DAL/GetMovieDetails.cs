﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TurnerBroadcastingSystem.Models;

namespace TurnerBroadcastingSystem.DAL
{
    /// <summary>
    /// This calss is used to implement methods to retrive movie details.
    /// </summary>
    public class GetMovieDetails
    {

        public SearchMovies GetMovieList(string movieName)
        {
            SearchMovies movies = new SearchMovies();
            List<String> moviesList=new List<string>();
            try
            {
                 string connectionString = ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString;

                 using (SqlConnection connection = new SqlConnection(connectionString))
                 {
                     string queryString = "select distinct TitleName from Title where TitleName like @TitleName";
                     SqlCommand command = new SqlCommand(queryString, connection);
                     command.Parameters.AddWithValue("@TitleName", string.Format("%{0}%",movieName));
                     connection.Open();
                     using (SqlDataReader movieDetailsReader = command.ExecuteReader())
                     {
                         while (movieDetailsReader.Read())
                         {
                             moviesList.Add(Convert.ToString(movieDetailsReader["TitleName"]));
                         }
                     }
                     movies.MoviesList = moviesList;
                     connection.Close();
                 }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return movies;
        }

        /// <summary>
        /// This method is used to retrive movie details based on given movie title.
        /// </summary>
        /// <param name="movieName">string</param>
        /// <returns>SearchMovies object</returns>
        public SearchMovies GetMovieDetailsList(string movieName)
        {
            SearchMovies movies = new SearchMovies();
            List<MovieDetails> movieDetailsList = new List<MovieDetails>();
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString;

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string queryString="select  t.TitleName,o.TitleNameLanguage,t.ReleaseYear,"
                                        +"s.Language,s.Description,a.AwardWon,a.AwardYear,a.Award,a.AwardCompany"
                                        +" from Title t join StoryLine s on t.TitleId =s.TitleId join Award a"
                                        +" on t.TitleId =a.TitleId join OtherName o on t.TitleId =o.TitleId"
                                        +"  where t.TitleId =(select distinct TitleId from Title where TitleName = @TitleName)";
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@TitleName", movieName);
                    connection.Open();
                    using (SqlDataReader movieDetailsReader = command.ExecuteReader())
                    {
                        while (movieDetailsReader.Read())
                        {
                            MovieDetails movieDetails=new MovieDetails();
                            movieDetails.TitleName = Convert.ToString(movieDetailsReader["TitleName"]);
                            movieDetails.ReleaseYear =Convert.ToInt32(movieDetailsReader["ReleaseYear"]);
                            movieDetails.Language = Convert.ToString(movieDetailsReader["Language"]);
                            //movieDetails.ArtistName = Convert.ToString(movieDetailsReader["Name"]);
                            movieDetails.Description = Convert.ToString(movieDetailsReader["Description"]);
                            movieDetails.AwardWon = Convert.ToBoolean(movieDetailsReader["AwardWon"]) == false ? true : false;
                            movieDetails.AwardYear = Convert.ToInt32(movieDetailsReader["AwardYear"]);
                            movieDetails.Award = Convert.ToString(movieDetailsReader["Award"]);
                            movieDetails.AwardCompany = Convert.ToString(movieDetailsReader["AwardCompany"]);
                            movieDetailsList.Add(movieDetails);
                        }
                        movies.MovieDetailsList = movieDetailsList;
                        connection.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

            return movies;
        }
    }
}