﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TurnerBroadcastingSystem.Models
{
    public class SearchMovies
    {
        [Required(ErrorMessage="Please enter movie title.")]
        public string Title
        {
            get;
            set;
        }

        public List<MovieDetails> MovieDetailsList
        {
            get;
            set;
        }

        public List<string> MoviesList
        {
            get;
            set;
        }
    }
}