﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurnerBroadcastingSystem.Models
{
    public class MovieDetails
    {
        public string TitleName
        {
            get;
            set;
        }

        public string ArtistName
        {
            get;
            set;
        }

        public string RoleType
        {
            get;
            set;
        }
        public int ReleaseYear
        {
            get;
            set;
        }

        public string Language
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public bool AwardWon
        {
            get;
            set;
        }

        public int AwardYear
        {
            get;
            set;
        }
        public string Award
        {
            get;
            set;
        }
        public string AwardCompany
        {
            get;
            set;
        }
    }
}