﻿using System.Web.Mvc;
using TurnerBroadcastingSystem.DAL;
using TurnerBroadcastingSystem.Models;

namespace TurnerBroadcastingSystem.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// This action return index page when we do GET request.
        /// </summary>
        /// <returns>ActionResult object</returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// This action is invoked when we submit the form by clicking searchMovies button.
        /// This action validate Title and invoke GetMovieList() method to get movies list
        /// </summary>
        /// <param name="searchMovies">SearchMovies object</param>
        /// <returns>ActionResult object</returns>
        [HttpPost]
        public ActionResult Index(SearchMovies searchMovies)
        {
            if (ModelState.IsValid)
            {
                SearchMovies searchMoviesToView = new SearchMovies();
                searchMoviesToView.MoviesList = new GetMovieDetails().GetMovieList(searchMovies.Title);
                return View(searchMoviesToView);
            }
            else
            {
                return View();
            }

        }

        /// <summary>
        /// This action is invoked when we click movie name and it retrives and display movie details
        /// </summary>
        /// <param name="title">string</param>
        /// <returns>ActionResult object</returns>
        [HttpGet]
        public ActionResult GetDetails(string title)
        {
            if (ModelState.IsValid)
            {
                SearchMovies searchMoviesToView = new SearchMovies();
                searchMoviesToView.MovieDetailsList = new GetMovieDetails().GetMovieDetailsList(title);
                return View(searchMoviesToView);
            }
            else
            {
                return View();
            }
            return View();
        }


    }
}
